export interface listarQuadrinhosInterface{
    id?: string;
    pageCount?: number;
    prices?: pricesInterface;
    thumbnail?: thumbnailInterface;
    title: string;
    modified: string;
}

export interface pricesInterface {
    price?: string;
}

export interface thumbnailInterface{
    extension: string;
    path: string;
}