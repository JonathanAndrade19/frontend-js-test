import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TelaDetalhamentoService } from './tela-detalhamento.service';
import { listarQuadrinhosInterface } from './../../models/listar-quadrinhos.model';
import { HeaderService } from './../../core/service/header.service';

@Component({
  selector: 'app-tela-detalhamento',
  templateUrl: './tela-detalhamento.component.html',
  styleUrls: ['./tela-detalhamento.component.scss']
})
export class TelaDetalhamentoComponent implements OnInit {

  @Input() public dataSource$!: number;
  public id: number;
  public dados: listarQuadrinhosInterface;
  public carrinho: Array<any> = [];
  
  constructor(
    private telaDetalhamento: TelaDetalhamentoService,
    private activeRouter: ActivatedRoute,
    private headerService: HeaderService
  ) { }

  ngOnInit(): void {
    this.getById();
  }

  public getById() {
    this.id = this.activeRouter.snapshot.params.id
    console.log(this.id);
    if (this.id) {
      this.telaDetalhamento.getByIdQuadrinhos(this.id).subscribe(
        (res) => {
          this.dados = res.data.results;
          console.log(res);
      }, (error: any) => {
        console.log(error);
      });
    }
  }

  public compraQuadrinho(dados: listarQuadrinhosInterface) {
    const playLoad = this.headerService.mountPayload(dados);
    this.headerService.setCarrinhos = playLoad;
  }

  public mascaraCep() {
    let exemplo = 'frete não calculado. Campo cep só de exemplo'
    alert(exemplo);
  }

}
