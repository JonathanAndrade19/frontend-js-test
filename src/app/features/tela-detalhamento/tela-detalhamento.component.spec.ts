import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaDetalhamentoComponent } from './tela-detalhamento.component';

describe('TelaDetalhamentoComponent', () => {
  let component: TelaDetalhamentoComponent;
  let fixture: ComponentFixture<TelaDetalhamentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelaDetalhamentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaDetalhamentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
