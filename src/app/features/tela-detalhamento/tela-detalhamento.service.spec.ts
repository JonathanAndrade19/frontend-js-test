import { TestBed } from '@angular/core/testing';

import { TelaDetalhamentoService } from './tela-detalhamento.service';

describe('TelaDetalhamentoService', () => {
  let service: TelaDetalhamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TelaDetalhamentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
