import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TelaDetalhamentoService {

  constructor(private http: HttpClient) { }

  public getByIdQuadrinhos(id: number): Observable<any>{
    return this.http.get(`https://gateway.marvel.com:443/v1/public/comics/${id}?ts=1000&apikey=4991f6b67c071092dbe67b36faae5681&hash=f0de242ee98d75c3990f9ec4147faf73`);
  }
}
