import { Component } from '@angular/core';
import { ListarQuadrinhoService } from './listar-quadrinho.service';

@Component({
  selector: 'app-listar-quadrinho',
  templateUrl: './listar-quadrinho.component.html',
  styleUrls: ['./listar-quadrinho.component.scss']
})
export class ListarQuadrinhoComponent{

  public listQuadrinhos = [];
  public offset = '0';
  public limit = '100';
 
  constructor(
    private listarQuadrinho: ListarQuadrinhoService
  ) {}

  ngOnInit(): void {
    this.getter();
  }

  public getter(): void{
    this.listarQuadrinho.getListarQuadrinhos(this.offset, this.limit)
      .subscribe(res => {
        console.log('listagem de Quadrinhos', res);
        this.listQuadrinhos = res.data.results;
      })
  }

}
