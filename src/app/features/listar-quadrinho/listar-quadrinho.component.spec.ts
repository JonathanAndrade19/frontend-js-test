import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarQuadrinhoComponent } from './listar-quadrinho.component';

describe('ListarQuadrinhoComponent', () => {
  let component: ListarQuadrinhoComponent;
  let fixture: ComponentFixture<ListarQuadrinhoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarQuadrinhoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarQuadrinhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
