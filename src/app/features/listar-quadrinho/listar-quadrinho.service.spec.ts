import { TestBed } from '@angular/core/testing';

import { ListarQuadrinhoService } from './listar-quadrinho.service';

describe('ListarQuadrinhoService', () => {
  let service: ListarQuadrinhoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ListarQuadrinhoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
