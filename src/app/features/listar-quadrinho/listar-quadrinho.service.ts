import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ListarQuadrinhoService {

  constructor(private http: HttpClient) { }

  public getListarQuadrinhos(offset, limit) {
    let api_url = 'https://gateway.marvel.com:443/v1/public/comics?ts=1000&apikey=4991f6b67c071092dbe67b36faae5681&hash=f0de242ee98d75c3990f9ec4147faf73'+'&offset='+offset+'&limit='+limit;

    return this.http.get<any>(api_url)
      .pipe(map((res: any) => {
        return res;
      }),
        retry(5));
  }

  public getlistQuadrinhosMarvel(api_url: string) {
   return this.http.get<any>(api_url)
      .pipe(map((res: any) => {
        return res;
      }),
        retry(5));
  }

}
