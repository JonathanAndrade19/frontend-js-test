import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutDosProdutosComponent } from './checkout-dos-produtos.component';

describe('CheckoutDosProdutosComponent', () => {
  let component: CheckoutDosProdutosComponent;
  let fixture: ComponentFixture<CheckoutDosProdutosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CheckoutDosProdutosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutDosProdutosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
