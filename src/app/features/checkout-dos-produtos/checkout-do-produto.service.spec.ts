import { TestBed } from '@angular/core/testing';

import { CheckoutDoProdutoService } from './checkout-do-produto.service';

describe('CheckoutDoProdutoService', () => {
  let service: CheckoutDoProdutoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CheckoutDoProdutoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
