import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { TelaDetalhamentoComponent } from './features/tela-detalhamento/tela-detalhamento.component';
import { CheckoutDosProdutosComponent } from './features/checkout-dos-produtos/checkout-dos-produtos.component';
import { ListarQuadrinhoComponent } from './features/listar-quadrinho/listar-quadrinho.component';
import { ListarQuadrinhoService } from './features/listar-quadrinho/listar-quadrinho.service';
import { HeaderComponent } from './core/header/header.component';

@NgModule({
  declarations: [
    AppComponent,
    ListarQuadrinhoComponent,
    TelaDetalhamentoComponent,
    CheckoutDosProdutosComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [ListarQuadrinhoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
