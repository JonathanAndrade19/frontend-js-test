import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CheckoutDosProdutosComponent } from './features/checkout-dos-produtos/checkout-dos-produtos.component';
import { ListarQuadrinhoComponent } from './features/listar-quadrinho/listar-quadrinho.component';
import { TelaDetalhamentoComponent } from './features/tela-detalhamento/tela-detalhamento.component';

const routes: Routes = [
  {
    path: "",
    component: ListarQuadrinhoComponent
  },
  {
    path: "tela-detalhamento/:id",
    component: TelaDetalhamentoComponent
  },
  {
    path: "checkout-dos-produtos",
    component: CheckoutDosProdutosComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
