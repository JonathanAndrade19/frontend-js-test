import { Component, OnInit } from '@angular/core';

import { HeaderService } from './../service/header.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public carrinho: any;

  constructor(private headerService: HeaderService) { }

  ngOnInit(): void {
    this.shop();
  }

  public shop(): void {
    this.carrinho = this.headerService.getCarrinhos;
    console.log(this.carrinho);
  }

}
