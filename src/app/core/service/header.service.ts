import { Injectable } from '@angular/core';

import { listarQuadrinhosInterface, thumbnailInterface } from './../../models/listar-quadrinhos.model';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {
  private _carrinhos: Array<listarQuadrinhosInterface> = []

  constructor() { }

  public get getCarrinhos(): Array<listarQuadrinhosInterface> {
    return this._carrinhos;
  }

  public set setCarrinhos(carrinhos: listarQuadrinhosInterface) {
    this._carrinhos.push(carrinhos);
  }

  public mountPayload(dados: listarQuadrinhosInterface) {
    const carrinhos = {
      id: dados.id,
      title: dados.title,
      modified: dados.modified,
      pageCount: dados.pageCount,
      thumbnail: {
        path: dados.thumbnail.path,
        extension: dados.thumbnail.extension
      },
      prices: {
        price: dados.prices[0].price
      }
    }    
    return carrinhos;
  }

}
