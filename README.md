# Desafio Phoebus ->

Meu Cronograma

## No Dia 1:

Foi o dia que eu li o desafio e comecei a separar em pequenas atividades para que no 3 e ultimo dia estivesse todas as funcionalidades solicitadas, estivesem feitas.

então criei o projeto e as pastas os fica todos os codigos, pasta Core onde fica a header, a nav do layout do sistema.

e criei a Features, onde fica as três telas, a tela de listagem dos quadrinhos, uma tela de detalhamento e a tela de checkout dos produtos selecionados, e seus services.

comecei a fazer o layout do header e comecei a estudar sobre a api da marvel. como se usa a api.

## No Dia 2:

Solucionei o problema que estava tendo para utilizar a api da marvel, era preciso criar um hash com a data, a chave privada e a chave publica (gastei quase que meu dia todo para conseguir resolver esse problema).

ainda conseguir fazer o layout da parte de listagem dos quadrinhos.

## no dia 3:

comecei o dia fazendo a tela de detalhamento dos quandrinhos, fazendo todo o chamado pela api. tive um problema que não estava consegundo trazer por id. depois de algumas tentativas conseguir. assim que conseguir fui finalizar o layout e assim finalizei a segunda tela.

infelizmente nao conseguir fazer a parte do carrinho de compra 'que seria a parte dos produtos selecionados, não conseguir levar os dados de uma pagina para outro como selecionados. os dados sempre sobrescrever o outro.

também não conseguir fazer a parte dos descontos.

## Como resolvir alguns problemas

tive um problema com a api, na hora de buscar os dados não sabia como gerar o hash que ele precisava, sempre me gerava um erro, como se eu não tivesse acesso, precisei pegar a minha keypublic e keyPrivate e date para gerar a minha hash.

outro problema que eu tive foi nas rotas. algo simples mais tive um pequeno problema usei o routerLink.

outro problema foi relacionado aos valores, que não estava vindo porque era um objeto dentro de um array criei um ngFor dentro de outro ngFor, so assim conseguir trazer os dados, outro problema ofi que os valores vinha em formato americano, conseguir resolver com o Currency:"BRL".

tive um pequeno problema com as imgs, elas vinham em um array com o Path e a extension separados precisei concatenar as duas para que assim a img pegasse.

outro problema que tive foi com datas que vinha em formato americano, usei o date: 'dd/MM/yyyy'.

peço desculpas por não ter finalizado todo o projeto, fiz o que conseguir, agradeço pela oportunidade e se puder gostaria de um feedback.

## problemas não resolvidos.

tentei trazer os dados para colocar no carrinho de comprar mas me deparei com um problema e não conseguir resolver, no tela-detalhamento.component.ts tem um metodo comprar quandrinhos la tem o this.headerService onde fica armazenado o valor dentro de um array com os valores, mas não consegui levar e colocar os valores na tela de produtos selecionados. (se caso na hora do teste pudessem verificar no campo Sources do navegador, você colocar um break point na linha 46 na tela-detalhamento. e inserir um quadrinho, ele até salva no this.headerService os valores mas não consigo levar a tela de produtos selecionados).

não consegui gerar cupom de desconto, pelo fato de não ter conseguido trazer os dados para a parte de Checkout-dos-produtos.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.0.

# como rodar o sistema.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
